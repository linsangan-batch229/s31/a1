/*

1. What built-in JS keyword is used to import packages?
answer: require

2. What Node.js module/package contains a method for server creation?
answer: http

3. What is the method of the http object responsible for creating a server using Node.js?
answer: createServer()

4. Between server and client, which creates a request?
answer: client

5. Between server and client, which creates a response?
answer: server

6. What is a runtime environment used to create stand-alone backend applications written in Javascript?
answer: nodeJS

7.What is the largest registry for Node packages?
answer: npm
*/